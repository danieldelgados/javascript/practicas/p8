	
/*Declarar variables globales que acceden a los divs del html*/
	var seg=document.querySelector('.segundero');
	var min=document.querySelector('.minutero');
	var hr=document.querySelector('.hora');

/*Evento de cargar la pagina primero*/
window.addEventListener('load',()=>{

/*Funcion que coge la hora y los transformamos en grados par que los estilos puedan funcionar*/

function hora(){
	var ahora=new Date();
	var segundos=ahora.getSeconds();
	var minutos=ahora.getMinutes();
	var hora=ahora.getHours(); 
	var segundosGrados=((segundos/60)*360)+90;
	var minutosGrados=((minutos/60)*360)+90;
	var horaGrados=((hora/12)*360)+90;
/*aqui utilizamos las variables (constantes)*/
	seg.style.transform="rotate("+segundosGrados+"deg)";
	min.style.transform="rotate("+minutosGrados+"deg)";
	hr.style.transform="rotate("+horaGrados+"deg)";
};
/*dejamos que el intervalo ocurra cada un segundo */
setInterval(hora, 1000);
});